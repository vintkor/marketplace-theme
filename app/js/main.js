$('document').ready(function(){

    $('#testimonals').owlCarousel({
        items: 1,
        loop: true,
        lazyLoad: true,
        autoplay: true,
        autoplayHoverPause: true,
        nav: true,
        navText: [
            "<div class='s8__left'><img src='../img/arrow-prev.png'></div>",
            "<div class='s8__right'><img src='../img/arrow-next.png'></div>"
        ]
    });

    $('#single-product-s2').owlCarousel({
        items: 5,
        loop: true,
        lazyLoad: true,
        autoplay: true,
        autoplayHoverPause: true,
        nav: false,
        dots: true
    });
   

    var galeries = $("div[id^='gallery']");
    $.each(galeries, function(){
        $('#' + $(this).attr('id')).photobox('a'); 
    });
    $('#redesign').styler();
    $('#niche_').styler();
    $('#remember, #account_type, #category, #country, #photo, #niche, #video, #main_image, #was_wasted, #price, #price_job, #payback').styler({
        fileBrowse: 'Выбрать файл'
    });

    $('#new_niche').selectpicker();

    var countClickNewPhoto = 1;
    $('#add-new-photo-btn').click(function(e){
        e.preventDefault();
        var oldInput = $('#add-new-photo').find('input'),
            newInput = oldInput.clone(),
            cloneIdValue = newInput.attr('id'),
            newIdValue = cloneIdValue + '-' + countClickNewPhoto;

        newInput.attr('id', newIdValue);
        $('.form__group__new').append(newInput);
        $('body').find('#' + newIdValue).styler({fileBrowse: 'Выбрать файл'});
        countClickNewPhoto++;
    });

    $('#add-benefits').click(function(e){
        e.preventDefault();
        var oldGroup = $('.new__benefits:first-child').clone(true).find("input, textarea").val("").end().find('.benefits__wrapper .benefits__choose_active').removeClass('benefits__choose_active').end();
        $('#wrapper__new__benefits').append(oldGroup);
    });

    $('#period').daterangepicker();

    $('.video__link').hover(function () {
        $(this).parent('.video').addClass('active');
    }, function () {
        $(this).parent('.video').removeClass('active');
    });

    // ============================= History =================================

    $('.history__button').click(function (e) {
        e.preventDefault();
        var historyYear = $(this).data('year');
        $('.history__text').removeClass('active');
        $('.history__button').removeClass('active');
        $(this).addClass('active');
        $('#' + historyYear).addClass('active animated fadeIn');
    });

    // ============================= Navigation =================================

    $('.nav-has-child').hover(
        function () {
            $(this).toggleClass('active').find('.child').toggleClass('bounceInDown');
        },
        function () {
            $(this).toggleClass('active').find('.child').toggleClass('bounceInDown');
        }
    );


    $('#city, #role, #lang').styler();

    function readURL(input, position) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var backgroundImage = 'background-image: url("' + e.target.result + '")';
                position.attr('style', backgroundImage);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#change__photo').click(function () {
        $('#photo_user').change(function () {
           readURL(this, $('.profile__avatar'));
       })
    });

    $('#change__cover').click(function () {
        $('#photo_cover').change(function () {
            readURL(this, $('.profile__top-background'));
        })
    });
// ========================== Footer dropdown ========================== //


(function () {
    var footerLists = document.getElementsByClassName('prefooter__list'),
        itemLists = document.getElementsByClassName('prefooter__item'),    
        itemHeight = itemLists[0].offsetHeight,
        maxItems = 6,
        maxListHeight = itemHeight * maxItems + 'px',
        btn = "<div class='accordeon'><button class='accordeon__button fa fa-arrow-circle-down'></button></div>",
        children = 0,
        buttonList = 0,
        btnClick = document.getElementsByClassName('accordeon__button'),
        thisList = '';
        
        for(key in footerLists) {
            children = 0;
            thisList = footerLists[key];
            for(items in thisList.children) {
                if(typeof(thisList.children[items]) == 'object') {
                    children++;
                }
            }
            
            if(children > maxItems){
                thisList.dataset.height = thisList.offsetHeight;
                thisList.style.height = maxListHeight;
                oldHTML = thisList.parentElement.innerHTML;
                thisList.parentElement.innerHTML = oldHTML + btn;
            }
        }


    function findChild(element) {
       var parent = element.parentElement.parentElement,
           child = parent.children;
           element.dataset.count = element.dataset.count++;
        for(classes in child) {
            if(typeof(child[classes]) == 'object' && child[classes].classList.contains('prefooter__list')){ 
                if(child[classes].style.height != maxListHeight) {
                    child[classes].style.height = maxListHeight;
                    element.classList.remove('fa-arrow-circle-up');
                    element.classList.add('fa-arrow-circle-down');
                }
                else {
                    child[classes].style.height = child[classes].dataset.height + 'px';
                    element.classList.remove('fa-arrow-circle-down');
                    element.classList.add('fa-arrow-circle-up');
                }
                
            }
        }
    }
    function animateHome() {
        if(".s6") {
            
        }
        var controller = new ScrollMagic.Controller();
        var aniTrigger = ".s6",
            aniOffset = -200;
    
        if($(window).width() < 768) {
            aniTrigger = ".s6__items-last";
            aniOffset = -200;
        }
    
        new ScrollMagic.Scene({
            triggerElement: aniTrigger,
            offset: aniOffset,
            duration: 600
        }).setTween(".s6__phone", {
            transform: "translateY(0)"
        })
            // .addIndicators({name: "Phone"})
            .addTo(controller);
    }


    function showFooter(e) {
        findChild(this);
    }

    $(document).on('click', '.accordeon__button', showFooter);
    
    // var _loop = function _loop(i) {
    //     btnClick[i].addEventListener('click', showFooter);
    // };

    // for (var i = 0; i < btnClick.length; i++) {
    //         _loop(i);
    // }
})();
});

plyr.setup();

// ========================== Animations ИНВЕСТИЦИИ ========================== //



// ========================== Загрука фото в галерею ========================== //


$('#galleryImage').on('change', fileUpload);
var form = $('.single-product-s3__add-inner'),  
    formData = new FormData(this);
    formURL = $('.single-product-s3__add-inner').attr('action');

function fileUpload() {
      $.ajax({
        method: "POST",
        url: formURL,
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(data) {
          
        },
        error: function(event){
          
        }
      });
}

// ========================== Установка id Benefits в скрытый инпут ========================== //

$('.benefits__choose').click(function(){
    var dataId = $(this).find('.benefits__img').data('id'),
        parent = $(this);
        grandParent = $(this).parent();
        
    grandParent.find('.benefits-input-img').val(dataId);
    grandParent.find('.benefits__choose').removeClass('benefits__choose_active');
    parent.addClass('benefits__choose_active');
});










    